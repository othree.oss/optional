
export function isOptional(optional: any): optional is Optional<any> {
    return Boolean(optional &&
        typeof optional.get === 'function' &&
        typeof optional.orElse === 'function' &&
        typeof optional.orThrow === 'function' &&
        typeof optional.map === 'function' &&
        typeof optional.mapAsync === 'function' &&
        typeof optional.orMap === 'function' &&
        typeof optional.orMapAsync === 'function' &&
        typeof optional.catch === 'function' &&
        typeof optional.catchAsync === 'function' &&
        typeof optional.getError === 'function' &&
        typeof optional.isEmpty === 'boolean' &&
        typeof optional.isPresent === 'boolean')
}

export function Try<T>(thunk: () => Optional<T> | T): Optional<T> {
    try {
        const value = thunk()
        if (isOptional(value)) {
            return value as Optional<T>
        }
        return Optional(value as T)
    } catch (error) {
        return Optional(undefined!, error)
    }
}

export async function TryAsync<T>(thunk: () => Promise<Optional<T> | T>): Promise<Optional<T>> {
    try {
        const value = await thunk()
        if (isOptional(value)) {
            return value as Optional<T>
        }
        return Optional(value as T)
    } catch (error) {
        return Optional(undefined!, error)
    }
}

export interface Maybe<T, E extends Error> {
    readonly get: () => T
    readonly orElse: (value: T) => T
    readonly orThrow: (error: E) => T
    readonly map: <A>(callback: (value: T) => Optional<A> | A) => Optional<A>
    readonly mapAsync: <A>(callback: (value: T) => Promise<Optional<A> | A>) => Promise<Optional<A>>
    readonly orMap: <A>(callback: () => Optional<A> | A) => Optional<A | T>
    readonly orMapAsync: <A>(callback: () => Promise<Optional<A> | A>) => Promise<Optional<A | T>>
    readonly getError: () => any
    readonly catch: <A>(callback: (error: E) => Optional<A> | A) => Optional<A | T>
    readonly catchAsync: <A>(callback: (error: E) => Promise<Optional<A> | A>) => Promise<Optional<A | T>>
    readonly isEmpty: boolean
    readonly isPresent: boolean
}

export type Optional<T> = Maybe<T, Error>

export function Empty<T>(error?: Error): Optional<T> {
    return Optional(undefined!, error)
}

export class EmptyOptionalError extends Error {
    static ERROR = 'EmptyOptionalError'

    constructor() {
        super('Optional is empty')

        this.name = EmptyOptionalError.ERROR
    }
}

export function Optional<T>(value: Optional<T> | T | undefined | null, error?: Error): Optional<T> {
    const getValue = (value: Optional<T> | T | undefined | null) => {
        if (isOptional(value)) {
            return (value as Optional<T>).isPresent ? (value as Optional<T>).get() : undefined
        }

        return value as T
    }
    const theValue = getValue(value)
    const theError = isOptional(value) && !error ? (value as Optional<T>).getError() : error
    const isEmpty = typeof theValue === 'undefined' || theValue === null
    const isPresent = !isEmpty

    return Object.freeze({
        get: () => {
            if (isEmpty) {
                if (!theError) {
                    throw new EmptyOptionalError()
                } else {
                    throw theError
                }
            }

            return theValue
        },
        orElse: function (elseValue: T) {
            if (isEmpty && !theError) {
                return elseValue
            }

            return this.get()
        },
        orThrow: (error: Error) => {
            if (isEmpty) {
                if (!theError) {
                    throw error
                }

                throw theError
            }

            return theValue
        },
        map: <A>(callback: (value: T) => Optional<A> | A): Optional<A> => {
            if (isPresent) {
                return Try(() => callback(theValue))
            }

            return Optional(undefined!, theError)
        },
        mapAsync: <A>(callback: (value: T) => Promise<Optional<A> | A>): Promise<Optional<A>> => {
            if (isPresent) {
                return TryAsync(() => callback(theValue))
            }

            return Promise.resolve(Optional(undefined!, theError))
        },
        orMap: <A>(callback: () => Optional<A> | A): Optional<A | T> => {
            if (isEmpty && !theError) {
                return Try(() => callback())
            }

            return Optional(theValue!, theError)
        },
        orMapAsync: <A>(callback: () => Promise<Optional<A> | A>): Promise<Optional<A | T>> => {
            if (isEmpty && !theError) {
                return TryAsync(() => callback())
            }

            return Promise.resolve(Optional(theValue!, theError))
        },
        getError: () => {
            return theError
        },
        catch: <A>(callback: (error: Error) => Optional<A> | A): Optional<A | T> => {
            if (isEmpty && theError) {
                return Try(() => callback(theError))
            }

            return Optional(theValue!, theError)
        },
        catchAsync: <A>(callback: (error: Error) => Promise<Optional<A> | A>): Promise<Optional<A | T>> => {
            if (isEmpty && theError) {
                return TryAsync(() => callback(theError))
            }

            return Promise.resolve(Optional(theValue!, theError))
        },
        isEmpty: isEmpty,
        isPresent: isPresent
    })
}
