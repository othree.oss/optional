import {Optional, Try, TryAsync, EmptyOptionalError, Empty, isOptional} from '../src'

describe('Optional', () => {
    it('should flatten optional values', () => {
        const maybeSomething = Optional(Optional(Optional(Optional('something'))))

        expect(maybeSomething.isPresent).toBeTruthy()
        expect(maybeSomething.get()).toEqual('something')
    })

    it('should catch an error', () => {
        const maybeError = Try(() => {
            throw new Error('KAPOW!')
        })
            .catch(e => {
                return 'Hello World!'
            })
        expect(maybeError.isPresent).toBeTruthy()
        expect(maybeError.get()).toEqual('Hello World!')
    })

    it('should catch an error and process an async callback', async () => {
        const maybeError = await Try(() => {
            throw new Error('KAPOW!')
        })
            .catchAsync(async e => {
                return 'Hello World!'
            })
        expect(maybeError.isPresent).toBeTruthy()
        expect(maybeError.get()).toEqual('Hello World!')
    })

    it('should return the original value if no error was thrown', () => {
        const maybeError = Try(() => {
            return 'Hello World!'
        })
            .catch(e => {
                return 'Good bye!'
            })
        expect(maybeError.isPresent).toBeTruthy()
        expect(maybeError.get()).toEqual('Hello World!')
    })

    it('should return the original value if no error was thrown when processing an async catch', async () => {
        const maybeError = await Try(() => {
            return 'Hello World!'
        })
            .catchAsync(async e => {
                return 'Good bye!'
            })
        expect(maybeError.isPresent).toBeTruthy()
        expect(maybeError.get()).toEqual('Hello World!')
    })

    it('should flatten an empty optional', () => {
        const maybeSomething = Optional(Optional(Optional(Optional(undefined))))

        expect(maybeSomething.isPresent).toBeFalsy()
    })

    it('should return true when value isPresent', () => {
        const maybeSomething = Optional('something')

        expect(maybeSomething.isPresent).toBeTruthy()
    })

    it('should return false if asked if isEmpty and value is present', () => {
        const isEmpty = Optional('something').isEmpty

        expect(isEmpty).toBeFalsy()
    })

    it('should return the value when getting it', () => {
        const value: Number = Optional(1337).get()

        expect(value).toEqual(1337)
    })

    it('should return the value if defined', () => {
        const theOther = {message: 'Other Value'}
        const value = Optional({message: 'Hello World'}).orElse(theOther)

        expect(value).toEqual({message: 'Hello World'})
    })

    it('should return the other value if the value is undefined', () => {
        const theOther = {message: 'Other Value'}
        const value = Optional<any>(undefined).orElse(theOther)

        expect(value).toEqual(theOther)
    })

    it('should throw the error if the optional is empty and in error state', () => {
        expect(() => Try(() => {throw new Error('KAPOW!'); return 'hello';}).orElse('Something' as any)).toThrow('KAPOW!')
    })

    it('should return the other value if the value is null', () => {
        const theOther = 'Other Value'
        const value = Optional<string>(null).orElse('Other Value')

        expect(value).toEqual(theOther)
    })

    it('should throw an Error when getting the value and it isEmpty', () => {
        expect(() => Optional(undefined).get()).toThrow('Optional is empty')
    })

    it('should throw the overridden default error when getting the value and it isEmpty', () => {
        expect(() => Optional(undefined, new Error('KAPOW!')).get()).toThrow('KAPOW!')
    })

    it('should get the overridden default error', () => {
        const optional = Optional(undefined, new Error('KAPOW!'))
        expect(optional.getError()).toStrictEqual(new Error('KAPOW!'))
    })

    it('should return the defined value', () => {
        expect(Optional('Hello').orThrow(new Error('KAPOW!'))).toEqual('Hello')
    })

    it('should throw an error if the value is not present', () => {
        expect(() => {
            Optional('Hello').map(_ => {throw new Error('KAPOW!')}).orThrow(new Error('POW!'))
        })
            .toThrow('KAPOW!')
    })

    it('should throw an error when no value is present', () => {
        expect(() => Optional(undefined).orThrow(new Error('KAPOW!')))
            .toThrow('KAPOW!')
    })

    it('should map the present value using the callback', () => {
        const originalValue = {message: 'Something'}

        const value = Optional(originalValue).map(theValue => theValue.message)

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual(originalValue.message)
    })

    it('should flatmap the present value using the callback', () => {
        const originalValue = {message: 'Something'}

        const value = Optional(originalValue).map(theValue => Optional(theValue.message))

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual(originalValue.message)
    })

    it('should map the present value using an async callback', async () => {
        const originalValue = {message: 'Something'}

        const value = await Optional(originalValue).mapAsync(async theValue => theValue.message)

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual(originalValue.message)
    })

    it('should mapAsync an empty optional', async () => {
        const value = await Empty<any>().mapAsync(async theValue => theValue.message)

        expect(value.isPresent).toBeFalsy()
    })

    it('should flatMap the present value using an async callback', async () => {
        const originalValue = {message: 'Something'}

        const value = (await Optional(originalValue).mapAsync(async theValue => Optional(theValue.message)))
            .map(x => {
                return {
                    message: x.toUpperCase()
                }
            })

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual({
            message: 'SOMETHING'
        })
    })

    it('should map to an empty optional if the original is empty', () => {
        const value = Empty<any>().map(theValue => theValue.message)

        expect(value.isEmpty).toBeTruthy()
    })

    it('should return the defined value when executing an orMap', () => {
        const value = Optional(1).orMap(() => 'hello world')

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual(1)
    })

    it('should leave the optional empty if it is in an error state when orMapping', () => {
        const value = Try(() => {throw new Error('KAPOW!')}).orMap(() => 'hello world')

        expect(value.isPresent).toBeFalsy()
        expect(value.getError()).toStrictEqual(new Error('KAPOW!'))
    })

    it('should map to a new value if the optional is empty', () => {
        const value = Empty<any>().orMap(() => 'hello world')

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual('hello world')
    })

    it('should return the defined value when executing an orMapAsync', async () => {
        const value = await Optional(1).orMapAsync(async () => 'hello world')

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual(1)
    })

    it('should leave the optional empty if it is in an error state when calling orMapAsync', async () => {
        const value = await Try(() => {throw new Error('KAPOW!')}).orMapAsync(async () => 'hello world')

        expect(value.isPresent).toBeFalsy()
        expect(value.getError()).toStrictEqual(new Error('KAPOW!'))
    })

    it('should map async to a new value if the optional is empty', async () => {
        const value = await Empty<any>().orMapAsync(async() => 'hello world')

        expect(value.isPresent).toBeTruthy()
        expect(value.get()).toEqual('hello world')
    })
})

describe('Try', () => {
    it('should try to execute the specified thunk and return an optional with the value', () => {
        const result = Try(() => 1234)

        expect(result.isPresent).toBeTruthy()
        expect(result.get()).toEqual(1234)
    })

    it('should try to execute the specified thunk and return an empty optional if it fails', () => {
        const result = Try(() => {
            throw new Error('KAPOW!')
        })

        expect(result.isEmpty).toBeTruthy()

        expect(() => result.get()).toThrow('KAPOW!')
    })

    it('should try to execute the specified thunk and return a promise with the optional value', async () => {
        const result = await TryAsync(() => Promise.resolve(1234))

        expect(result.isPresent).toBeTruthy()
        expect(result.get()).toEqual(1234)
    })

    it('should try to execute the specified thunk and return a promise with an empty optional if it failed', async () => {
        const result = await TryAsync(() => Promise.reject(Error('KAPOW!')))

        expect(result.isEmpty).toBeTruthy()
        expect(() => result.get()).toThrow('KAPOW!')
    })
})

describe('Empty', () => {
    it('should return an empty optional', () => {
        const maybeValue = Empty()

        expect(maybeValue.isEmpty).toBeTruthy()
    })
})

describe('EmptyOptionalError', () => {
    it('should create an EmptyOptionalError', () => {
        const error = new EmptyOptionalError()
        expect(error.message).toEqual('Optional is empty')
        expect(error.name).toEqual(EmptyOptionalError.ERROR)
    })
})

describe('isOptional', () => {
    it('should return true if the specified parameter is an optional', () => {
        const maybeMessage = Optional('Hello World!')

        expect(isOptional(maybeMessage)).toBeTruthy()
    })

    it('should return false if the parameter is not an optional', () => {
        expect(isOptional({})).toBeFalsy()
    })
})
